# Block Racer

Block Racer is a simple endurance-based retro arcade game. This game challenges the user to avoid all incoming obstacles as long as they can.

# Prerequisites

* [AVR Studio 4](https://www.microchip.com/mplab/avr-support/avr-and-sam-downloads-archive)
- [Hapsim](http://www.helmix.at/hapsim/)

# Instructions:

* The Game will display LCD with the text "START" and a zero score, LED with 3 lights on, and three buttons (START, UP, and DOWN)
* Press the "START" button to start the game
* The player is shown with the "X" sign and the obstacles are shown with the "#" sign
* Try to avoid incoming obstacles by pressing the UP and DOWN button
* Every time the player hits the obstacle, the player's life will be decreased by one (one LED will be switched off)
* When the player runs out of lives (All LED is off), the LCD will display "GAME OVER" and the player's final score


## Authors

* Jeffrey (1706039843)
* Jonathan Christopher Jakub (1706040151)
* Muhammad Ardivan Satrio Nugroho (1706025371)

## Acknowledgements

* POK CSUI 2018
* Private Mentor : Ezra Riskiatama